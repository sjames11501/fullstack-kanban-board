export type KanbanTask = {
  id: number | undefined;
  createdDate: string | undefined;
  modifiedDate: string | undefined;
  name: string;
  tags?: {
    id: number;
    name: string;
    color: string;
  }[];
  categoryId?: number | undefined;
};
