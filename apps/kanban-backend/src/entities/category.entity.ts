import { Entity, Column, OneToMany } from 'typeorm';
import { BaseTable } from './base-table.entity';
import { KanbanTask } from './task.entity';

@Entity()
export class KanbanCategory extends BaseTable {
  @Column({ name: 'Name' })
  name: string;

  @OneToMany(() => KanbanTask, (task) => task.category, {
    eager: true,
    lazy: false,
    cascade: ['insert', 'update', 'remove', 'soft-remove', 'recover'],
    onDelete: 'CASCADE',
  })
  tasks: KanbanTask[];
}
