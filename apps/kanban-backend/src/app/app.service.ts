import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { KanbanCategory } from '../entities/category.entity';

@Injectable()
export class AppService {

  getData(): { message: string } {
    return { message: 'Welcome to kanban-backend!' };
  }
}
