import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
} from '@nestjs/common';
import { Connection } from 'typeorm';
import { GenericRepository } from '../repositories/generic-repository';
import { IRepository } from '../repositories/repository.interface';
import { KanbanTask, KanbanCategory } from '../entities';
import { SeedHelper } from '../helpers/SeedHelper';
import { KanbanTag } from '../entities/tag.entity';

@Controller()
export class AppController {
  private readonly _categoryRepository: IRepository<KanbanCategory>;
  private readonly _taskRepository: IRepository<KanbanTask>;
  private readonly _tagRepository: IRepository<KanbanTag>;
  // Inject TypeOrm Connection
  constructor(private readonly _connection: Connection) {
    this._categoryRepository = new GenericRepository(
      KanbanCategory,
      _connection
    );
    this._taskRepository = new GenericRepository(KanbanTask, _connection);
    this._tagRepository = new GenericRepository(KanbanTag, _connection);
  }

  @Put('category/seed')
  async seedData() {
    const dbResult = await this._categoryRepository.save(
      SeedHelper.getCategories()
    );

    if (dbResult) return true;
  }
  // Delete task
  @Delete('task/:id')
  async deleteTask(@Param() params): Promise<boolean> {
    const dbResult = await this._taskRepository.delete(params.id as number);
    if (dbResult) return true;
    throw new BadRequestException('An error has occurred.');
  }

  @Post('category/:name')
  async addCategory(@Param() params): Promise<KanbanCategory | boolean> {
    try {
      const newCategory = new KanbanCategory();
      newCategory.name = params.name;
      const dbResult = await this._categoryRepository.saveOne(newCategory);
      return dbResult;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
  // Delete category
  @Delete('category/:id')
  async deleteCategory(@Param() params): Promise<boolean> {
    if (isNaN(params.id)) throw new BadRequestException('invalid cId.');
    const dbResult = await this._categoryRepository.delete(params.id);
    if (dbResult) return true;
    throw new BadRequestException('An error has occurred.');
  }

  // Add tags to an existing post
  @Post('task/tags')
  async addTags(@Body() body: { tags: string[]; taskId: number }) {
    if (isNaN(body.taskId) || body.tags.length < 1) {
      throw Error('Invalid request');
    }
    try {
      const dbTags = await this._tagRepository.findAll();
      const dbTask = await this._taskRepository.find(body.taskId);
      for (const tag of body.tags) {
        // Check if the selected tag already exists on this task - if so, skip to the next tag
        const tagExistsOnTask = dbTask.tags.find(
          (e) => e.name.toLowerCase() === tag.toLowerCase()
        );
        if (tagExistsOnTask) continue;
        // Check if this tag already exist in the database
        const existingTag = dbTags.find(
          (item) => item.name.toLowerCase() === tag
        );
        if (existingTag) {
          dbTask.tags.push(existingTag);
        } else {
          const newTag = {
            name: tag,
          } as KanbanTag;
          const dbAddTag = await this._tagRepository.saveOne(newTag);
          dbTask.tags.push(dbAddTag);
        }

        if (dbTask.tags.length > 0) {
          const dbSaveResult = await this._taskRepository.saveOne(dbTask);
          return dbSaveResult;
        }
        throw new BadRequestException('An error has occurred.');
      }
    } catch (error) {
      console.log(error);
      throw new BadRequestException('An error has occurred.');
    }
    throw new BadRequestException('An error has occurred.');
  }

  @Post('task')
  async createTask(@Body() _task: KanbanTask) {
    try {
      // Map the category to this new Task
      const category = new KanbanCategory();
      category.id = _task.categoryId;
      _task.category = category;
      const dbResult = await this._taskRepository.saveOne(_task);
      if (!dbResult.id) throw Error('An error has occurred.');
      delete dbResult.category;
      return dbResult;
    } catch (error) {
      console.log(error);
      throw new BadRequestException('An error has occurred.');
    }
  }

  @Post('/category/:name')
  async createCategory(@Param() params): Promise<KanbanCategory> {
    try {
      const dbCategories = await this._categoryRepository.findAll();
      const categoryExist = dbCategories.find(
        (c) => c.name.toLowerCase() === params.name
      );
      if (categoryExist) throw Error('This category already exists');

      // Map the category to this new Task
      const category = new KanbanCategory();
      category.name = params.name;
      const dbResult = await this._categoryRepository.saveOne(category);
      if (!dbResult.id) throw Error('An error has occurred.');
      return dbResult;
    } catch (error) {
      console.log(error);
      throw new BadRequestException('An error has occurred.');
    }
  }

  // Update a task's categoryId
  @Patch('tasks/:id/category/:categoryId')
  async updateTask(@Param() params): Promise<boolean> {
    try {
      if (isNaN(params.id) || isNaN(params.categoryId)) {
        throw Error('Invalid request');
      }
      const dbCategory = new KanbanCategory();
      dbCategory.id = params.categoryId;
      const dbObj = new KanbanTask();
      dbObj.id = params.id;
      dbObj.category = dbCategory;
      const dbResult = await this._taskRepository.update(dbObj);
      if (dbResult) return true;
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error);
    }
  }
  @Get()
  async getData(): Promise<KanbanCategory[]> {
    try {
      const dbResult = await this._categoryRepository.findAll();
      return dbResult;
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error);
    }
  }
}
