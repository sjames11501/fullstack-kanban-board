# Full Stack Code Challenge Checklist

**Checklist Backend:**

- [x] Generic Repository
- [x] POST endpoint for creating a new task
- [x] POST endpoint for adding tags to an existing task
- [x] GET endpoint for retrieving the Kanban board
- [x] PATCH endpoint for changing a task's category
- [x] DELETE endpoint for deleting a task

**Front-end Checklist**

- [x] Add new task
- [x] Add new category
- [x] Delete task
- [x] Delete category
- [x] Drag and drop tasks
- [x] BackendService to call the API

