import { Container, Row, Col, Card, Badge } from 'react-bootstrap';
import styled from 'styled-components';

import { SetStateAction, useState } from 'react';

const StyledInput = styled.input`
  border: 0px;
  margin-left: 1rem;
  font-size: 0.85rem;
  float: left;
  width: 85%;
`;

const AddNewCategoryHeader = styled.h2`
  font-size: 1rem;
  font-size: 0.75rem;
  line-height: 2rem;
  float: left;
`;
const SubmitButton = styled.button`
  font-size: 0.75rem;
`;

// Interface for props
interface IAddCategoryFormProps {
  addCategoryHandler: (name: string) => void;
}

const AddCategoryComponent = (props: IAddCategoryFormProps) => {
  const [categoryName, setCategoryName] = useState('');
  function handleChange(e: {
    preventDefault: () => void;
    target: { value: SetStateAction<string> };
  }) {
    e.preventDefault();

    setCategoryName(e.target.value);
  }

  return (
    <form
      className="add-task-form"
      onSubmit={(e) => {
        e.preventDefault();
        props.addCategoryHandler(categoryName);
        // Reset the form
        setCategoryName('');
      }}
    >
      <Col>
        <Col style={{ float: 'left' }}>
          <AddNewCategoryHeader> Add new Category:</AddNewCategoryHeader>
        </Col>
        <Col style={{ float: 'left' }}>
          <StyledInput
            type="text"
            className="form-control"
            placeholder="Category name"
            value={categoryName}
            onChange={handleChange}
          />
        </Col>
        <Col style={{ float: 'left' }}>
          <SubmitButton className="btn btn-dark">Add Category</SubmitButton>
        </Col>
      </Col>
    </form>
  );
};

export default AddCategoryComponent;
