import { KanbanTask } from './kanban-task.type';

export type KanbanCategory = {
  id: number;
  droppableId: string;
  name: string;
  tasks: KanbanTask[];
};
