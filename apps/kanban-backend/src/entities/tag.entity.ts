import { Entity, Column, JoinTable, ManyToMany } from 'typeorm';
import { BaseTable } from './base-table.entity';
import { KanbanTask } from './task.entity';

@Entity()
export class KanbanTag extends BaseTable {
  @Column()
  name: string;

  @Column()
  color: string;
  @ManyToMany(() => KanbanTask, { eager: false })
  @JoinTable()
  tasks?: KanbanTask[];
}

export default KanbanTag;
