import { SetStateAction, useState } from 'react';
import { Badge, Button } from 'react-bootstrap';
import styled from 'styled-components';
import { KanbanTask } from '../types/kanban-task.type';

// Styled Components
const Wrapper = styled.div`
  float: left;
  width: 100%;
  text-align: center !important;
  border: 2px dashed #dfdfdf;
  border-radius: 4px;
`;
const FormWrapper = styled.div<{ isVisible: string }>`
  text-align: center !important;
  display: ${(props) =>
    (props.isVisible as unknown as boolean) ? 'block' : 'none'};
`;

const AddTaskToggleButton = styled.span`
  line-height: 3rem;
  color: #9b9b9b;
  cursor: pointer;
  font-weight: 500;
  background-color: transparent;
`;

const AddTaskButton = styled.button`
  width: 100%;
  margin-top: 1rem;
`;

const StyledInput = styled.input`
  border: 0px;
  width: 100%;
`;

// Interface for props
interface IAddTaskFormProps {
  addTaskHandler: (task: KanbanTask) => void;
}
const AddTaskForm = (props: IAddTaskFormProps) => {
  const [show, setShow] = useState(false);
  const [tagId, setTagId] = useState(0);
  const [taskName, setTaskName] = useState('');
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [selectedTags, setSelectedTags] = useState<
    { id: number; name: string; color: string }[]
  >([]);

  const availableTags = [
    { id: 1, name: 'High Priority', color: 'danger' },
    { id: 2, name: 'Medium Priority', color: 'warning' },
    { id: 3, name: 'Test tag', color: 'info' },
    { id: 4, name: 'Low Priority', color: 'dark' },
  ];

  function handleChange(e: {
    preventDefault: () => void;
    target: { value: SetStateAction<string> };
  }) {
    e.preventDefault();

    setTaskName(e.target.value);
  }

  const toggle = () => {
    if (show) {
      setShow(false);
      return;
    }
    setShow(true);
  };
  const getDisplayText = () => {
    if (show) return 'Add Task (-)';
    return 'Add Task (+)';
  };

  const newTask: KanbanTask = {
    id: undefined,
    categoryId: undefined,
    name: taskName,
    createdDate: undefined,
    modifiedDate: undefined,
    tags: selectedTags,
  };

  const addTag = (id: number) => {
    const tagAlreadySelected = selectedTags.find((tag) => tag.id === id);
    const tag = availableTags.find((t) => t.id === id);
    if (!tag) return;
    if (!tagAlreadySelected) {
      setSelectedTags([...selectedTags, tag]);
      return;
    }
    // If the tag is already selected - remove it from selectedTags
    const index = selectedTags.findIndex((e) => e.id === id);

    setSelectedTags([
      ...selectedTags.slice(0, index),
      ...selectedTags.slice(index + 1),
    ]);
  };

  // Return the className for a tag
  const getTagColor = (id: number) => {
    const tag = availableTags.find((t) => t.id === id);
    const tagAlreadySelected = selectedTags.find((tag) => tag.id === id);

    if (!tagAlreadySelected) {
      return 'secondary';
    }
    return tag?.color;
  };

  return (
    <Wrapper>
      <AddTaskToggleButton onClick={() => toggle()}>
        {getDisplayText()}
      </AddTaskToggleButton>
      <FormWrapper isVisible={show as unknown as string}>
        <form
          className="add-task-form"
          onSubmit={(e) => {
            e.preventDefault();
            if (taskName.length < 4) {
              alert('Please enter a valid task name');
              return;
            }
            props.addTaskHandler(newTask);
            // Reset the form
            setTaskName('');
            setSelectedTags([]);
          }}
        >
          {availableTags.map((tag) => (
            <Badge
              pill
              style={{ cursor: 'pointer' }}
              onClick={() => addTag(tag.id)}
              key={tag.id}
              bg={getTagColor(tag.id)}
            >
              {tag.name}
            </Badge>
          ))}
          <StyledInput
            type="text"
            className="form-control"
            placeholder="Task name"
            value={taskName}
            onChange={handleChange}
          />
          <AddTaskButton className="btn btn-primary">Add Task</AddTaskButton>
        </form>
      </FormWrapper>
    </Wrapper>
  );
};

export default AddTaskForm;
