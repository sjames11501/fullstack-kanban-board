import '../styles.css';
import { Row, Col, Card } from 'react-bootstrap';
import styled from 'styled-components';
import TaskCard from './task-card';
import AddTaskComponent from './add-task';
import { useState, useEffect } from 'react';
import { KanbanCategory } from '../types/kanban-category.type.';
import { KanbanTask } from '../types/kanban-task.type';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { useMutation, useQueryClient } from 'react-query';
import { addTask, deleteTask } from '../services/backend.service';
// #region STYLED COMPONENTS
const DeleteWrapper = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  color: #858585;
`;
const CategoryWrapper = styled.div`
  position: relative;
  display: flex;
  height: auto;
  background-color: #eef2f9;
  box-shadow: 0px 0px 10px 0px #d2d7e0;
  word-wrap: break-word;
  border: 1px solid rgba(0, 0, 0, 0.125);
  border-radius: 0.25rem;
`;
const CategoryTitle = styled.h5`
  font-size: 1.25rem;
  color: #7e7e7e;
  margin-bottom: 1rem;
`;
const CategoryBody = styled.div`
  border-top: 4px solid #eee;
  flex: 1 1 auto;
  padding: 1rem 1rem;
  border-top: 4px solid #12d1a347;
`;
// #endregion

// Interface for props
interface IKanbanCategoryCard {
  data: KanbanCategory;
  deleteHandler: (id: number) => void;
}

const KanbanCategoryCard = (props: IKanbanCategoryCard) => {
  const queryClient = useQueryClient();
  const [category, setCategory] = useState<KanbanCategory>(props.data);
  const deleteTaskMutation = useMutation(deleteTask);
  const addTaskMutation = useMutation(addTask);
  useEffect(() => {
    setCategory(props.data);
  }, [props.data]);

  // #region Handlers
  const deleteTaskHandler = async (id: number) => {
    try {
      await deleteTaskMutation.mutateAsync(id);
      queryClient.invalidateQueries();
    } catch (error) {
      console.log(error);
    }
  };

  const addTaskHandler = async (newTask: KanbanTask) => {
    if (newTask.name.match(/^[^A-Z0-9]*$/i)) {
      alert('invalid task name');
      return;
    }

    if (newTask.name.length < 2 || newTask.name.length > 30) {
      alert('invalid task name');
      return;
    }
    try {
      newTask.categoryId = category.id;
      await addTaskMutation.mutateAsync(newTask);
      queryClient.invalidateQueries();
    } catch (error) {
      console.log(error);
    }
  };
  // #endregion

  return (
    <Col lg={12}>
      <CategoryWrapper>
        <CategoryBody>
          <CategoryTitle>{props.data.name}</CategoryTitle>
          <DeleteWrapper>
            <FontAwesomeIcon
              icon={faTimes}
              onClick={() => props.deleteHandler(category.id)}
            />
          </DeleteWrapper>

          {(category as KanbanCategory).tasks.length < 1 && (
            <p>This category has no tasks. Be the first person to add one!</p>
          )}
          {(category as KanbanCategory).tasks.map((task) => {
            return (
              <TaskCard
                key={task.id}
                deleteTaskHandler={deleteTaskHandler}
                task={task}
              />
            );
          })}

          <AddTaskComponent addTaskHandler={addTaskHandler} />
        </CategoryBody>
      </CategoryWrapper>
    </Col>
  );
};
export default KanbanCategoryCard;
