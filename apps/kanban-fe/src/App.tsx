import '../src/styles.css';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Container } from 'react-bootstrap';
import KanbanBoard from './components/kanban-board';

const App = () => {
  // Create a client
  const queryClient = new QueryClient();

  return (
    <QueryClientProvider client={queryClient}>
      <Container>
        <KanbanBoard queryKey="" />
      </Container>
    </QueryClientProvider>
  );
};

export default App;
