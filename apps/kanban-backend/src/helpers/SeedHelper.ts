import { KanbanCategory } from '../entities/category.entity';
import { KanbanTask } from '../entities/task.entity';
import { KanbanTag } from '../entities/tag.entity';

enum TagColor {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  DANGER = 'danger',
  INFO = 'info',
}
export class SeedHelper {
  static getCategories = () => {
    const backlogCategory = new KanbanCategory();
    backlogCategory.name = 'Backlog';
    const todoCategory = new KanbanCategory();
    todoCategory.name = 'To-do';
    const doneCategory = new KanbanCategory();
    doneCategory.name = 'Completed';

    // Add tasks to each board
    backlogCategory.tasks = SeedHelper.getTasksBoard1();
    todoCategory.tasks = SeedHelper.getTasksBoard2();
    doneCategory.tasks = SeedHelper.getTasksBoard3();

    const categories: KanbanCategory[] = [
      backlogCategory,
      todoCategory,
      doneCategory,
    ];

    return categories;
  };

  static getTasksBoard1 = () => {
    const tag = new KanbanTag();
    tag.name = 'High Priority';
    tag.color = TagColor.DANGER;
    const task1 = new KanbanTask();
    task1.name = 'Prepare ad campaign';

    task1.tags = [tag];

    const task2 = new KanbanTask();
    task2.name = 'Walk the dog';

    task2.tags = [tag];

    const task3 = new KanbanTask();
    task3.name = 'Document new API';
    task3.tags = [tag];

    return [task1, task2, task3];
  };

  static getTasksBoard2 = () => {
    const task1 = new KanbanTask();
    task1.name = 'Create facebook page';

    const task2 = new KanbanTask();
    task2.name = 'Produce financial report for Q4';

    return [task1, task2];
  };

  static getTasksBoard3 = () => {
    const tag = new KanbanTag();
    tag.name = 'Low Priority';
    tag.color = TagColor.INFO;
    const task1 = new KanbanTask();
    task1.name = 'Create facebook page';
    task1.tags = [tag];

    const task2 = new KanbanTask();
    task2.name = 'Produce financial report for Q3';
    task2.tags = [tag];

    const task3 = new KanbanTask();
    task3.name = 'Produce financial report for Q1';

    const task4 = new KanbanTask();
    task4.name = 'Create newsletter template';

    return [task1, task2, task3, task4];
  };
}
