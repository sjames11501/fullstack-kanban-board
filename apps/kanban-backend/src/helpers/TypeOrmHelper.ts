import { getConnectionOptions, getConnection } from 'TypeORM';

export class TypeOrmHelper {
  static getDbConnectionOptions = async () => {
    const options = await getConnectionOptions(
      process.env.NODE_ENV || 'development'
    );
    return {
      ...options,
      name: 'defau;t',
    };
  };

  getDbConnection = async (connectionName: string) => {
    return await getConnection(connectionName);
  };

  runDbMigrations = async (connectionName: string) => {
    const conn = await this.getDbConnection(connectionName);
    await conn.runMigrations();
  };
}
