import { Module } from '@nestjs/common';
import * as entities from '../entities';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      entities: [...Object.values(entities)], // Use barrel file for importing entities
      autoLoadEntities: true,
      synchronize: true, // Don't use this in production - otherwise you can lose production data.
      database: 'kanban-db.sqlite3',
      type: 'sqlite',
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  // Inject our TypeOrm connection into the AppModule
  constructor(private connection: Connection) {}
}
