import {
  Entity,
  Column,
  ManyToOne,
  JoinTable,
  ManyToMany,
  JoinColumn,
} from 'typeorm';
import { BaseTable } from './base-table.entity';
import { KanbanCategory } from './category.entity';
import { KanbanTag } from './tag.entity';

@Entity()
export class KanbanTask extends BaseTable {
  @Column()
  name: string;

  @ManyToMany(() => KanbanTag, { eager: true, cascade: ['update', 'insert'] })
  @JoinTable()
  tags: KanbanTag[];

  @ManyToOne(() => KanbanCategory, (category) => category.tasks, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'categoryId' })
  category: KanbanCategory;

  @Column({ nullable: false })
  categoryId: number;
}
