// #region IMPORTS
import '../styles.css';
import { Row, Col, Card } from 'react-bootstrap';
import logo from '../assets/logo.png';
import styled from 'styled-components';
import { useState, useEffect, SetStateAction } from 'react';
import { KanbanCategory } from '../types/kanban-category.type.';
import AddCategoryComponent from './add-category';
import KanbanCategoryCard from './kanban-category-card';
import {
  DragDropContext,
  DraggableLocation,
  Droppable,
  DropResult,
} from 'react-beautiful-dnd';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import {
  deleteCategory,
  addCategory,
  fetchKanbanBoard,
  seedMockData,
  updateTaskCategory,
} from '../services/backend.service';
// #endregion

// #region STYLED COMPONENTS
const CategoryList = styled.div`
  display: block;
`;
const Logo = styled.img`
  max-width: 400px;
  position: relative;
  left: -20px;
`;

const SeedButton = styled.button`
  font-size: 0.75rem;
  margin-left: 0.5rem;
`;
// #endregion

interface IKanbanBoardProps {
  queryKey: string;
}

const KanbanBoard = (props: IKanbanBoardProps) => {
  const queryClient = useQueryClient();
  const { data, error, isError, isLoading, isSuccess } = useQuery<
    KanbanCategory[]
  >(props.queryKey, fetchKanbanBoard, {
    cacheTime: 60 * 1000 * 10,
    staleTime: 60 * 1000,
  });
  const [categories, setCategories] = useState<KanbanCategory[]>([]);

  // #region Mutation Handlers
  const deleteCategoryMutation = useMutation(deleteCategory);
  const addCategoryMutation = useMutation(addCategory);
  const updateTaskCategoryMutation = useMutation(updateTaskCategory);
  // #endregion
  const onDragEnd = async (result: DropResult) => {
    // Dropped outside the list
    if (!result.destination) return;
    const sInd = +parseInt(result.source.droppableId.replace('droppable-', ''));
    // Destination CategoryId
    const dInd = +parseInt(
      result.destination.droppableId.replace('droppable-', '')
    );
    const arrClone = JSON.parse(JSON.stringify(categories)) as KanbanCategory[];
    const sourceCategory = arrClone.find(
      (c) => c.id === sInd
    ) as KanbanCategory;

    const destinationCategory = arrClone.find(
      (c) => c.id === dInd
    ) as KanbanCategory;

    const removedItemIndex = sourceCategory.tasks.findIndex(
      (i) => i.id === result.source.index
    );
    const destinationItemIndex = destinationCategory.tasks.findIndex(
      (i) => i.id === (result.destination as DraggableLocation).index
    );
    const [removed] = sourceCategory.tasks.splice(removedItemIndex, 1);

    if (destinationCategory.id !== sourceCategory.id) {
      try {
        await updateTaskCategoryMutation.mutateAsync({
          taskId: removed.id as number,
          categoryId: destinationCategory.id,
        });
        queryClient.invalidateQueries();
      } catch (error) {
        console.log(error);
      }
    }

    // destinationCategory.tasks.splice(destinationItemIndex, 0, removed);
    // setCategories([...arrClone]);
  };

  useEffect(() => {
    if (isSuccess) {
      const newData = data as KanbanCategory[];
      setCategories(newData);
    }
  }, [isSuccess, data]);

  // #region React Query States
  if (isLoading) {
    return <div>Loading...</div>;
  }
  if (isError) {
    const message = (error as { message: string }).message;
    return <div>Error! {message}</div>;
  }
  // #endregion

  // #region Handlers
  const deleteCategoryHandler = async (id: number) => {
    try {
      await deleteCategoryMutation.mutateAsync(id);
      queryClient.invalidateQueries();
    } catch (error) {
      console.log(error);
    }
  };

  const seedHandler = async () => {
    await seedMockData();
    queryClient.invalidateQueries();
  };
  const addCategoryHandler = async (name: string) => {
    if (name.length < 4 || name.length > 30) {
      alert('Please enter a valid category name');
      return;
    }

    // Check if the category already exists
    const existingCategory = categories.find(
      (c) => c.name.toLowerCase() === name.toLowerCase()
    );

    if (existingCategory) {
      alert('Please provide a unique category name');
      return;
    }

    try {
      await addCategoryMutation.mutateAsync(name);
      queryClient.invalidateQueries();
    } catch (error) {
      console.log(error);
    }
  };
  // #endregion

  return (
    <DragDropContext onDragEnd={(result) => onDragEnd(result)}>
      <Row className="text-center">
        <Logo src={logo}></Logo>
      </Row>
      <Row>
        <Col>
          <AddCategoryComponent addCategoryHandler={addCategoryHandler} />

          {categories.length < 1 && (
            <SeedButton className="btn btn-dark" onClick={() => seedHandler()}>
              Seed Mock Data
            </SeedButton>
          )}
        </Col>
      </Row>

      <Row>
        {categories.length < 1 && (
          <p>
            This board doesn't have any categories. Be the first person to add
            one!
          </p>
        )}

        {categories.map((item) => (
          <Droppable key={item.id} droppableId={`droppable-${item.id}`}>
            {(provided, snapshot) => (
              <CategoryList
                className="col-lg-3 col-xs-12"
                ref={provided.innerRef}
                style={{
                  opacity: snapshot.isDraggingOver ? '0.8' : '1.00',
                }}
                {...provided.droppableProps}
              >
                <KanbanCategoryCard
                  key={item.id}
                  deleteHandler={deleteCategoryHandler}
                  data={item}
                />
                {provided.placeholder}
              </CategoryList>
            )}
          </Droppable>
        ))}
      </Row>
    </DragDropContext>
  );
};

export default KanbanBoard;
