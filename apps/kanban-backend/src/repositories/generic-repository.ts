import 'reflect-metadata';
import { Connection, Repository } from 'typeorm';
import { IRepository } from '../repositories/repository.interface';
import { BaseTable } from '../entities/base-table.entity';

type ObjectType<T> = { new (): T };

export class GenericRepository<T> implements IRepository<T> {
  private readonly _repository: Repository<T>;

  constructor(
    private readonly _type: ObjectType<T>,
    private readonly _connection: Connection
  ) {
    this._repository = this._connection.getRepository(_type) as Repository<T>;
  }

  // Find a single <Type> by it's ID
  public delete = async (id: number): Promise<boolean> => {
    const dbResult = await this._repository?.delete(id);

    if (dbResult.affected > 0) {
      return true;
    }
    return false;
  };

  // Find a single <Type> by it's ID
  public find = async (id: number): Promise<T> => {
    const dbResult = await this._repository?.findOne(id);
    return dbResult as T;
  };

  public findAll = async (): Promise<T[]> => {
    const dbResult = await this._repository.find();
    return dbResult;
  };

  public findWithRelations = async (
    relations: Array<string>,
    id: number | undefined
  ): Promise<T[]> => {
    const result = await this._repository.find({
      relations: [relations.join()],
      where: id ? { id: id } : undefined,
    });
    return result;
  };

  public findByField = async (
    fieldName: string,
    fieldValue: string
  ): Promise<T> => {
    const manager = this._connection.manager;
    const rawSQL = `SELECT *
                        FROM  ${this._type.name}
                        WHERE ${this._type.name}.${fieldName}= ?;`;

    // Pass field value as parameter
    const rawData = (await manager?.query(rawSQL, [fieldValue])) as T;
    return rawData;
  };

  // Use this for partial object updates - the provided object must have its ID set
  public update = async (obj: T): Promise<boolean> => {
    // Throw error
    if (!this._repository.hasId(obj)) {
      throw Error(
        'Unable to update record.The provided record does not have a valid Id.'
      );
    }

    await this._repository?.update((obj as unknown as BaseTable).id, obj);
    return true;
  };

  // Save an array of <Type>
  public save = async (obj: T[]): Promise<T[]> => {
    for (const item of obj) {
      if (this._repository.hasId(item)) {
        console.log(item);
        throw Error(
          'Unable to insert record.The provided record already has an id set.Did you mean to call doUpdate()?'
        );
      }
    }

    const dbResult = await this._repository?.save(obj);
    return dbResult;
  };

  // Save a <Type> object
  public saveOne = async (obj: T): Promise<T> => {
    const dbResult = await this._repository?.save(obj);
    return dbResult;
  };
}
