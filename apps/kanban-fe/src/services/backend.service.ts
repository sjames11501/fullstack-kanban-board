import axios from 'axios';
import { KanbanCategory } from '../types/kanban-category.type.';
import { KanbanTask } from '../types/kanban-task.type';

// #region Endpoint URLs
const fetchBoardUrl = 'http://localhost:3333/api';
const deleteCategoryUrl = 'http://localhost:3333/api/category';
const addCategoryUrl = 'http://localhost:3333/api/category';
const addTaskUrl = 'http://localhost:3333/api/task';
const deleteTaskUrl = 'http://localhost:3333/api/task';
const seedUrl = 'http://localhost:3333/api/category/seed';
const updateTaskCategoryUrl =
  'http://localhost:3333/api/tasks/{taskId}/category/{categoryId}';
// #endregion

export async function fetchKanbanBoard() {
  const { data } = await axios.get(fetchBoardUrl);
  return data;
}

export const updateTaskCategory = async (obj: {
  taskId: number;
  categoryId: number;
}) => {
  let url = updateTaskCategoryUrl.replace('{taskId}', obj.taskId.toString());
  url = url.replace('{categoryId}', obj.categoryId.toString());
  const response = await axios.patch(`${url}`);
  return response as unknown as boolean;
};

export const addTask = async (data: KanbanTask) => {
  console.log('BackendService - addTask()');
  const response = await axios.post(`${addTaskUrl}`, data);
  return response as unknown as KanbanTask;
};
export const deleteTask = async (id: number) => {
  console.log('BackendService - deleteTask()');
  const response = await axios.delete(`${deleteTaskUrl}/${id}`);
  return response;
};

export const addCategory = async (name: string) => {
  console.log('BackendService - addCategory()');
  const response = await axios.post(`${addCategoryUrl}/${name}`);
  return response as unknown as KanbanCategory;
};
export const deleteCategory = async (id: number) => {
  let response;
  console.log('BackendService - deleteCategory()');
  try {
    response = await axios.delete(`${deleteCategoryUrl}/${id}`);
  } catch (error) {
    console.log(error);
  }
  return response;
};

export const seedMockData = async () => {
  try {
    const response = await axios.put(seedUrl);
    if (response) return true;
  } catch (error) {
    console.log(error);
    return false;
  }
  return false;
};
