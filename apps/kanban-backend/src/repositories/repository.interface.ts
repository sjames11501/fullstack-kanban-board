export interface IRepository<T> {
  findAll(): Promise<T[]>;
  find(id: number): Promise<T>;
  save(obj: T[]): Promise<T[]>;
  saveOne(obj: T): Promise<T>;
  update(obj: T): Promise<boolean>;
  delete(id: number): Promise<boolean>;
}
