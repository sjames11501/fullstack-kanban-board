import { Container, Row, Col, Card, Badge } from 'react-bootstrap';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { KanbanTask } from '../types/kanban-task.type';
import { useState } from 'react';
import { Draggable } from 'react-beautiful-dnd';

// #region STYLED COMPONENTS
const TaskName = styled.h6`
  font-size: 1rem;
  line-height: 1rem;
  font-weight: 400;
  margin-top: 6px;
  width: 100%;
  color: #7e7e7e;
`;
const DeleteWrapper = styled.div`
  position: absolute;
  top: 10%;
  right: 4%;
  display: block;
  color: #858585;
  display: none;
`;

const Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  padding: grid;
  background-color: #fff;
  border-radius: 0.25rem;
  margin-bottom: 1rem;
  &:hover ${DeleteWrapper} {
    display: block;
  }
`;

const TaskCardBody = styled.div`
  flex: 1 1 auto;
  padding: 0.5rem 0.5rem;
`;

// #endregion

const getItemStyle = (isDragging: boolean) => ({
  opacity: isDragging ? '0.2' : '1.00',
  border: isDragging ? '4px dashed #CCC' : '',
  display: 'flex',
  justifyContent: 'space-around',
});

interface ITaskCard {
  deleteTaskHandler: (id: number, categoryId: number) => void;
  task: KanbanTask;
}
const TaskCard = (props: ITaskCard): JSX.Element => {
  const tagItems = props.task.tags
    ? props.task.tags.map((e) => (
        <Badge key={e.id} bg={e.color}>
          {e.name}
        </Badge>
      ))
    : undefined;

  return (
    <Draggable
      key={props.task.id as number}
      draggableId={`draggable-${props.task.id}`}
      index={props.task.id as number}
    >
      {(provided, snapshot) => (
        <Wrapper
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <div style={getItemStyle(snapshot.isDragging)}>
            <TaskCardBody>
              {tagItems}
              <TaskName>{props.task.name}</TaskName>
              <DeleteWrapper>
                <FontAwesomeIcon
                  icon={faTimes}
                  onClick={() =>
                    props.deleteTaskHandler(
                      props.task.id as number,
                      props.task.categoryId as number
                    )
                  }
                />
              </DeleteWrapper>
            </TaskCardBody>
          </div>
        </Wrapper>
      )}
    </Draggable>
  );
};

export default TaskCard;
